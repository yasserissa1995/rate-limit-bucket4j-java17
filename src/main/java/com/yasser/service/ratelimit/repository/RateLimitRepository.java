package com.yasser.service.ratelimit.repository;

import com.yasser.service.ratelimit.domain.RateLimit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RateLimitRepository extends JpaRepository<RateLimit, String> {

}
