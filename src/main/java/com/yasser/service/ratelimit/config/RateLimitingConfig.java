package com.yasser.service.ratelimit.config;

import io.github.bucket4j.distributed.jdbc.PrimaryKeyMapper;
import io.github.bucket4j.mssql.Bucket4jMSSQL;
import io.github.bucket4j.mssql.MSSQLSelectForUpdateBasedProxyManager;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.time.Duration;

import static io.github.bucket4j.distributed.ExpirationAfterWriteStrategy.basedOnTimeForRefillingBucketUpToMax;

@Configuration
@AllArgsConstructor
public class RateLimitingConfig {


   private final DataSource dataSource;

   @Bean
   public MSSQLSelectForUpdateBasedProxyManager<String> proxyManager() {

      return new Bucket4jMSSQL.MSSQLSelectForUpdateBasedProxyManagerBuilder<>(dataSource, PrimaryKeyMapper.STRING).expirationAfterWrite(basedOnTimeForRefillingBucketUpToMax(Duration.ofMinutes(2))).table("TRACKING_RATE_LIMIT").idColumn("PK_KEY_NAME").stateColumn("STATE").build();
   }
}