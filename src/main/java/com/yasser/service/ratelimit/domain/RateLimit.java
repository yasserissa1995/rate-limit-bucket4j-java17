package com.yasser.service.ratelimit.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "TRACKING_RATE_LIMIT")
@Data
public class RateLimit {

   @Id
   @Column(name = "PK_KEY_NAME")
   private String id;

   @Column(name = "EXPIRES_AT")
   private Long expiresAt;

   @Column(name = "STATE")
   private byte[] state;
}
