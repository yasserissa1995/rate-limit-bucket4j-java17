package com.yasser.service.ratelimit.service;

import com.yasser.service.ratelimit.domain.RateLimit;
import com.yasser.service.ratelimit.dto.RateLimitDto;
import com.yasser.service.ratelimit.repository.RateLimitRepository;
import io.github.bucket4j.BucketConfiguration;
import io.github.bucket4j.ConsumptionProbe;
import io.github.bucket4j.mssql.MSSQLSelectForUpdateBasedProxyManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.time.Duration.ofMinutes;

@Service
@Slf4j
@RequiredArgsConstructor
public final class RateLimitService {

   private final MSSQLSelectForUpdateBasedProxyManager<String> proxyManager;

   private static final int BATCH_SIZE = 500;
   private final BucketConfiguration SMS = BucketConfiguration.builder().addLimit(limit -> limit.capacity(20).refillIntervally(10, ofMinutes(1))).build();
   private final BucketConfiguration EMAIL = BucketConfiguration.builder().addLimit(limit -> limit.capacity(5).refillGreedy(1, ofMinutes(5))).build();
   private final RateLimitRepository rateLimitRepository;

   public RateLimitDto tryToSendLoginOtpSms(String username) {
      log.info("request to try to consume send login otp sms : {}", username);

      return buildRateLimitDto(proxyManager.builder().build("SMS" + username, SMS).tryConsumeAndReturnRemaining(1));
   }


   /* use another bucket with same configuration of sms bucket */
   public RateLimitDto tryToSendChangePasswordOtpSms(String username) {
      log.info("request to try to consume send change password otp sms : {}", username);

      return buildRateLimitDto(proxyManager.builder().build("SMS" + username, SMS).tryConsumeAndReturnRemaining(1));
   }

   public boolean isAvailableTokensSms(String username) {
      log.info("request to check available Tokens sms: {}", username);

      return proxyManager.builder().build("SMS_" + username, SMS).getAvailableTokens() > 0L;
   }

   /* use another bucket with same configuration of sms bucket */
   public RateLimitDto tryToSendLoginOtpEmail(String username) {
      log.info("request to try to consume send change password otp email : {}", username);

      return buildRateLimitDto(proxyManager.builder().build("EMAIL" + username, EMAIL).tryConsumeAndReturnRemaining(1));
   }

   private RateLimitDto buildRateLimitDto(ConsumptionProbe consumptionProbe) {

      RateLimitDto bucketDto = new RateLimitDto();

      if (consumptionProbe.isConsumed()) {
         bucketDto.setAvailableTokens(consumptionProbe.getRemainingTokens());
         return bucketDto;
      }

      bucketDto.setReFillMinutes(BigDecimal.valueOf(TimeUnit.NANOSECONDS.toSeconds(consumptionProbe.getNanosToWaitForRefill())).divide(BigDecimal.valueOf(60), 2, RoundingMode.CEILING).multiply(BigDecimal.valueOf(60)).doubleValue());
      bucketDto.setAvailableTokens(0L);
      bucketDto.setErrorMsg("You have exhausted the number of unsuccessful attempts. Please try again later");

      return bucketDto;
   }

   // once per day at 4:30 morning
   @Scheduled(cron = "*/30 * * * * *")
   public void scheduleFixedDelayTask() {
      log.info("Start remove expiry rate-limit");

      System.out.println("proxyManager.isExpireAfterWriteSupported() = " + proxyManager.isExpireAfterWriteSupported());


      List<RateLimit> a = rateLimitRepository.findAll();

      a.stream().forEach(rateLimit -> {
         System.out.println(LocalDateTime.ofInstant(Instant.ofEpochMilli(rateLimit.getExpiresAt()), ZoneId.systemDefault()));
      });

      int removedCount = proxyManager.removeExpired(1);
      log.info("End remove expiry rate-limit : {}", removedCount);
   }
}